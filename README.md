## POC Flight Status APP Angular JS 


### Install Dependencies

```
npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new
folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the angular framework files

*Note that the `bower_components` folder would normally be installed in the root folder but
angular-seed changes this location through the `.bowerrc` file.  Putting it in the app folder makes
it easier to serve the files by a webserver.*

### Run the Application

We have preconfigured the project with a simple development web server.  The simplest way to start
this server is:

```
npm start
```

Now browse to the app at diferent languages

- http://localhost:8000/app/#/es_cl/search
- http://localhost:8000/app/#/es_ar/search
- http://localhost:8000/app/#/es_pe/search
- http://localhost:8000/app/#/pt_br/search
- http://localhost:8000/app/#/fr_fr/search
- http://localhost:8000/app/#/en_us/search
- http://localhost:8000/app/#/it_it/search
- etc.
