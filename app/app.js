'use strict';

/**
 * flightStatusApp Angular App
 * @type {module|*}
 */
var app = angular.module('flightStatusApp',
    ['ngRoute',
        'ngSanitize',
        'ui.bootstrap',
        'pascalprecht.translate',
        'searchController',
        'headerLan',
        'footerLan',
        'i18n']);

/**
 * Config app
 * $routeProvider
 * $translateProvider
 *
 */
app.config(['$routeProvider', '$translateProvider', function ($routeProvider, $translateProvider) {
    $routeProvider.otherwise({redirectTo: '/es_cl/search'});
    $translateProvider.useLoader('i18nService');
    $translateProvider.useLoaderCache(true);
    $translateProvider.useSanitizeValueStrategy('escaped');
}]);





