'use strict';
angular.module('footerLan', [])

    .directive('footerLan', function() {
        return {
            restrict: 'E',
            templateUrl: 'components/directives/footer-lan/footer-lan-template.html'
        };
    });

