'use strict';
angular.module('headerLan', [])

    .directive('headerLan', function() {
        return {
            restrict: 'E',
            scope: {
                home: '=home'
            },
            templateUrl: 'components/directives/header-lan/header-lan-template.html'
        };
    });

