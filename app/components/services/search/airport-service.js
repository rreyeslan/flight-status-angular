'use strict';

/**
 * module searchService
 * @author rodrigo.reyes@lan.com
 *
 */
angular.module('searchService', [])

    .service('airportService', function ($http, $q, $filter, i18nHelper) {
        var airports = [];

        this.getLatamAirportsService = function (home) {
            var deferred = $q.defer();

            var homeObject = i18nHelper.getLanguageCountryByHome(home);

            var url = "http://www.lan.com/ws/api/common/airports/1.0/rest/" +
                "applicationName/flight_status/" +
                "portal/personas/" +
                "language/"
                    .concat(angular.uppercase(homeObject.language))
                    .concat("/country/").concat(angular.uppercase(homeObject.country));

            var requestParams = {
                portal: 'personas',
                application: 'flight_status',
                country: angular.uppercase(homeObject.country),
                language: angular.uppercase(homeObject.language),
                channel: 'WEB'
            }
            $http(angular.extend({
                url: url,
                params: requestParams,
                method: 'GET'
            })).success(function (airports) {
                deferred.resolve(airports);
            }).error(function () {
                deferred.reject("i18n error");
            });

            return deferred.promise;
        };

    });
