'use strict';

/**
 * module i18n
 * @author rodrigo.reyes@lan.com
 */
angular.module('i18n', [])


    .service('i18nHelper', function () {
        this.getHome = function (language, country) {
            return language + '_' + country;
        };

        this.getLanguageCountryByHome = function (home) {
            var homeArray = home.split('_');
            return {language: homeArray[0], country: homeArray[1]};
        };
    })



    .factory('i18nService', function ($http, $q, $sce, i18nHelper, $filter) {
        // return loaderFn
        return function (options) {
            var deferred = $q.defer();

            var home = i18nHelper.getLanguageCountryByHome(options.key);

            var url = "http://www.lan.com/ws/api/" +
                "i18n/" +
                "v1/" +
                "rest/" +
                "dictionaries/" +
                "flight_status::2.0::"+home.language+"::"+home.country+"::personas";

            var requestParams = {
                portal: 'personas',
                application: 'flight_status',
                country: angular.uppercase(home.country),
                language: angular.uppercase(home.language),
                channel: 'WEB'
            }
            $http(angular.extend({
                url: url,
                params: requestParams,
                method: 'GET'
            }, options.$http)).success(function (data) {
                deferred.resolve(data.data.dictionaries[0].translations);
            }).error(function () {
                deferred.reject(options.key);
            });

            return deferred.promise;
        };
    });