'use strict';


/**
 * Controller searchController
 * @author rodrigo.reyes@lan.com
 */
angular.module('searchController', ['ngRoute', 'searchService'])

/**
 *
 */
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/:isoLanguage*\_:isoCountry/search', {
            templateUrl: 'search/search.html',
            controller: 'SearchController'
        });
    }])

/**
 *
 */
    .controller('SearchController',
    ['$translate',
        '$scope',
        '$rootScope',
        '$routeParams',
        'i18nHelper',
        'airportService',
        function ($translate,
                  $scope,
                  $rootScope,
                  $routeParams,
                  i18nHelper,
                  airportService) {
            var home = i18nHelper.getHome($routeParams.isoLanguage, $routeParams.isoCountry);

            var _airports = [];
            $scope.airportOrigin;
            $scope.airportDestination;

            $translate.use(home);
            $rootScope.home = $translate.proposedLanguage();

            airportService.getLatamAirportsService(home).then(function (value) {
                angular.forEach(value.data, function (item) {
                    _airports.push(item.cityName + ', ' + item.countryName + ' (' + item.airportCode + ')');
                });
                $scope.airports = _.compact(_airports);
            });

            $scope.justDemo = function() {
                alert("Sorry!! es solo una demo");
                return false;
            }

        }]);
